# Stepper

A simple stepper demo app just to show changing date dynamically.

## Version 3

Update:

- Added a stepper with a slider:
  - Slider sets step (initial value 1)
  - Counter counts the days from today according to the step set by the stepper
  - Reset button is displayed only if the stepper value is bigger than 1 and counter value is not 0.

## Version 1 and 2

Steps: changes step of count.

Count: changes number of days based on count:

- if the step is 0, then days do not change when you press count buttons.
- if the step is 1, then the days are changed by 1: 1, 2, 3... by pressing count + or -
- if the step is 2, then days are changed with step 2: 2, 4, 6... by pressing count + or -

![Stepper2](./public/assets/stepper2.png)
![Stepper2](./public/assets/stepper3.png)
![Stepper1](./public/assets/stepper.png)

(3 versions, initial was the initial version, code less structured)
#Challenge 2
