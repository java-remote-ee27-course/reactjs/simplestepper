import { useState } from "react";

function Stepper2() {
  const [step, setStep] = useState(1);

  return (
    <div>
      <div>
        <input
          type="range"
          min={0}
          max={10}
          value={step}
          onChange={(e) => setStep(Number(e.target.value))}
          className="slider"
        />
        {step}
      </div>

      <Counter step={step} resetCounter={() => setStep(1)} />
    </div>
  );
}

function Counter({ step, resetCounter }) {
  const [count, setCount] = useState(0);
  const date = new Date();

  date.setDate(date.getDate() + count);

  function increaseCount() {
    setCount((count) => count + step);
  }

  function decreaseCount() {
    setCount((count) => count - step);
  }

  function handleReset() {
    setCount(0);
    resetCounter();
  }

  return (
    <>
      <div>
        <button onClick={decreaseCount}>-</button>
        <input
          type="text"
          value={count}
          onChange={(e) => setCount(Number(e.target.value))}
        />

        <button onClick={increaseCount}>+</button>
      </div>
      <div className="counter">
        {count === 0 && <span> Today is </span>}
        {count > 0 && <span>{count} days from today: </span>}
        {count < 0 && <span>{Math.abs(count)} days ago was: </span>}
        <span>{date.toDateString()}</span>
      </div>

      {(count !== 0 || step > 1) && <Button reset={handleReset} />}
    </>
  );
}

function Button({ reset }) {
  return (
    <div>
      <button className="btn" onClick={reset}>
        Reset
      </button>
    </div>
  );
}

export default Stepper2;
