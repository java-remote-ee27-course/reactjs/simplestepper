import { useState } from "react";

function Stepper1() {
  const [step, setStep] = useState(0);
  return (
    <div>
      <div>
        <button onClick={() => setStep((s) => s - 1)}>-</button>
        <span> Step: {step} </span>
        <button onClick={() => setStep((s) => s + 1)}>+</button>
      </div>

      <Counter step={step} />
    </div>
  );
}

function Counter({ step }) {
  const [count, setCount] = useState(0);
  const date = new Date();
  date.setDate(date.getDate() + count);

  function increaseCount() {
    setCount((count) => count + step);
  }

  function decreaseCount() {
    setCount((count) => count - step);
  }

  return (
    <>
      <div>
        <button onClick={decreaseCount}>-</button>
        <span> Count: {count} </span>
        <button onClick={increaseCount}>+</button>
      </div>
      <div className="counter">
        {count === 0 && <span> Today is </span>}
        {count > 0 && <span>{count} days from today: </span>}
        {count < 0 && <span>{Math.abs(count)} days ago was: </span>}
        <span>{date.toDateString()}</span>
      </div>
    </>
  );
}

export default Stepper1;
