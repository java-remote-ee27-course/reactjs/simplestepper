import "./App.css";
import Stepper2 from "./Stepper2.js";
import Stepper1 from "./Stepper1.js";
import Stepper0 from "./Stepper0.js";
import Heading from "./Heading";

function App() {
  return (
    <div className="container">
      <Heading name="Stepper (the newest version)" />
      <Stepper2 />
      <div className="space"></div>
      <Heading name="Stepper" />
      <Stepper1 />
      <div className="space"></div>
      <Heading name="Initial stepper" />
      <Stepper0 />
    </div>
  );
}
export default App;
