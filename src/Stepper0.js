import { useState } from "react";

function Stepper0() {
  const [step, setStep] = useState(0);
  const [count, setCount] = useState(0);
  const [dateStr, setDateStr] = useState(new Date().toDateString());
  const date = new Date();

  function increaseCount() {
    setCount((count) => count + step);
    date.setDate(date.getDate() + count + step);
    setDateStr(date.toDateString());
  }

  function decreaseCount() {
    setCount((count) => count - step);
    date.setDate(date.getDate() + count - step);
    setDateStr(date.toDateString());
  }

  return (
    <div>
      <div>
        <button onClick={() => setStep((s) => s - 1)}>-</button>
        <span> Step: {step} </span>
        <button onClick={() => setStep((s) => s + 1)}>+</button>
      </div>

      <div>
        <button onClick={decreaseCount}>-</button>
        <span> Count: {count} </span>
        <button onClick={increaseCount}>+</button>
      </div>

      {count === 0 && <div className="counter"> Today is {dateStr}</div>}
      {count > 0 && (
        <div className="counter">
          {count} days from today is {dateStr}
        </div>
      )}

      {count < 0 && (
        <div className="counter">
          {Math.abs(count)} days ago was {dateStr}
        </div>
      )}
    </div>
  );
}

export default Stepper0;
